package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.EditorCommand;
import pl.artapps.ksiegozbiorsb3.model.Editor;

@Component
public class EditorToEditorCommand implements Converter<Editor, EditorCommand> {

    @Synchronized
    @Nullable
    @Override
    public EditorCommand convert(Editor editor) {
        if (editor == null) {
            return null;
        } else {
            final EditorCommand editorCommand = new EditorCommand();
            editorCommand.setId(editor.getId().toString());
            editorCommand.setName(editor.getName());
            editorCommand.setCity(editor.getCity());
            return editorCommand;
        }
    }
}
