package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.TagCommand;
import pl.artapps.ksiegozbiorsb3.model.Tag;

@Component
public class TagToTagCommand implements Converter<Tag, TagCommand> {

    @Synchronized
    @Nullable
    @Override
    public TagCommand convert(Tag tag) {
        if (tag == null) {
            return null;
        } else {
            final TagCommand tagCommand = new TagCommand();
            tagCommand.setId(tag.getId());
            tagCommand.setName(tag.getName());
            return tagCommand;
        }
    }
}
