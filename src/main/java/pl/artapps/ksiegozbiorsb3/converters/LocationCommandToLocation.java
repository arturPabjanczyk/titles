package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.LocationCommand;
import pl.artapps.ksiegozbiorsb3.model.Location;

@Component
public class LocationCommandToLocation implements Converter<LocationCommand, Location> {

    @Synchronized
    @Nullable
    @Override
    public Location convert(LocationCommand locationCommand) {
        if (locationCommand == null) {
            return null;
        } else {
            final Location location = new Location();
            location.setId(locationCommand.getId());
            location.setName(locationCommand.getName());
            return location;
        }
    }
}
