package pl.artapps.ksiegozbiorsb3.converters;

import lombok.*;
import org.springframework.core.convert.converter.*;
import org.springframework.lang.*;
import org.springframework.stereotype.*;
import pl.artapps.ksiegozbiorsb3.commands.*;
import pl.artapps.ksiegozbiorsb3.model.*;

@Component
public class TitleToTitleCommand implements Converter<Title, TitleCommand> {

    @Synchronized
    @Nullable
    @Override
    public TitleCommand convert(Title title) {
        if (title == null) {
            return null;
        } else {
            final TitleCommand titleCommand = new TitleCommand();
            titleCommand.setId(title.getId().toString());
            titleCommand.setName(title.getName());
            titleCommand.setAuthors(title.getAuthors());
            titleCommand.setTranslators(title.getTranslators());
            titleCommand.setCatNumber(title.getCatNumber());
            titleCommand.setTitleSecondPart(title.getTitleSecondPart());
            titleCommand.setOriginalTitle(title.getOriginalTitle());
            titleCommand.setNumberOfPages(title.getNumberOfPages());
            titleCommand.setReleaseYear(title.getReleaseYear());
            titleCommand.setLink(title.getLink());
            titleCommand.setWhenRead(title.getWhenRead());
            titleCommand.setEditor(title.getEditor());
            titleCommand.setIsbn(title.getIsbn());
            titleCommand.setNotes(title.getNotes());
            titleCommand.setReview(title.getReview());
            titleCommand.setSection(title.getSection());
            titleCommand.setTags(title.getTags());
            return titleCommand;
        }
    }
}
