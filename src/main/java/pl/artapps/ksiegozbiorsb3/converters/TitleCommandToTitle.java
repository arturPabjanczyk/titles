package pl.artapps.ksiegozbiorsb3.converters;

import lombok.*;
import org.springframework.core.convert.converter.*;
import org.springframework.lang.*;
import org.springframework.stereotype.*;
import pl.artapps.ksiegozbiorsb3.commands.*;
import pl.artapps.ksiegozbiorsb3.model.*;
import pl.artapps.ksiegozbiorsb3.repositories.*;

import java.util.*;

@Component
public class TitleCommandToTitle implements Converter<TitleCommand, Title> {

    private final TitleRepository titleRepository;

    public TitleCommandToTitle(TitleRepository titleRepository) {
        this.titleRepository = titleRepository;
    }

    @Synchronized
    @Nullable
    @Override
    public Title convert(TitleCommand titleCommand) {
        Title title = new Title();
        if (titleCommand == null) {
            return null;
        } else {
            if (titleCommand.getId() != null && !("".equals(titleCommand.getId()))) {
                Optional<Title> optionalTitle1 = titleRepository.findById(Long.valueOf(titleCommand.getId()));
                if (optionalTitle1.isPresent()) {
                    title = optionalTitle1.get();
                }
            }

            title.setName(titleCommand.getName());
            if (titleCommand.getOvertitle() != null && !"".equals(titleCommand.getOvertitle())) {
                Optional<Title> optionalTitle = titleRepository.findById(Long.valueOf(titleCommand.getOvertitle()));
                if (!optionalTitle.isPresent()) {
                    throw new RuntimeException("Tytuł nie odnaleziony");
                } else {
                    Title title1 = optionalTitle.get();
//                    Set<Title> subtitles = title1.getSubtitles();
//                    subtitles.add(title);
//                    title1.setSubtitles(subtitles);
//                    title.setOvertitle(title1);
                }
            }

            title.setAuthors((titleCommand.getAuthors()));
            title.setTranslators((titleCommand.getTranslators()));
            title.setCatNumber(titleCommand.getCatNumber());
            title.setTitleSecondPart(titleCommand.getTitleSecondPart());
            title.setOriginalTitle(titleCommand.getOriginalTitle());
            title.setNumberOfPages(titleCommand.getNumberOfPages());
            title.setReleaseYear(titleCommand.getReleaseYear());
            title.setLink(titleCommand.getLink());
            title.setWhenRead(titleCommand.getWhenRead());
            title.setEditor(titleCommand.getEditor());
            title.setIsbn(titleCommand.getIsbn());
            title.setNotes(titleCommand.getNotes());
            title.setReview(titleCommand.getReview());
            title.setSection(titleCommand.getSection());
            title.setTags(titleCommand.getTags());
            return title;
        }
    }
}
