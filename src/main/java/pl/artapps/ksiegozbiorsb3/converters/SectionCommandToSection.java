package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.SectionCommand;
import pl.artapps.ksiegozbiorsb3.model.Section;

@Component
public class SectionCommandToSection implements Converter<SectionCommand, Section> {

    @Synchronized
    @Nullable
    @Override
    public Section convert(SectionCommand sectionCommand) {
        if (sectionCommand == null) {
            return null;
        } else {
            final Section section = new Section();
            section.setId(sectionCommand.getId());
            section.setName(sectionCommand.getName());
            return section;
        }
    }
}
