package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.FormatCommand;
import pl.artapps.ksiegozbiorsb3.model.Format;

@Component
public class FormatToFormatCommand implements Converter<Format, FormatCommand> {

    @Synchronized
    @Nullable
    @Override
    public FormatCommand convert(Format format) {
        if (format == null) {
            return null;
        } else {
            final FormatCommand formatCommand = new FormatCommand();
            formatCommand.setId(format.getId());
            formatCommand.setName(format.getName());
            return formatCommand;
        }
    }
}
