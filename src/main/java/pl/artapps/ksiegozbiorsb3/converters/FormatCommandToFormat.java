package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.FormatCommand;
import pl.artapps.ksiegozbiorsb3.model.Format;

@Component
public class FormatCommandToFormat implements Converter<FormatCommand, Format> {

    @Synchronized
    @Nullable
    @Override
    public Format convert(FormatCommand formatCommand) {
        if (formatCommand == null) {
            return null;
        } else {
            final Format format = new Format();
            format.setId(formatCommand.getId());
            format.setName(formatCommand.getName());
            return format;
        }
    }
}
