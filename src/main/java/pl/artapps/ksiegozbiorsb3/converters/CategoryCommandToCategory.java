package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.CategoryCommand;
import pl.artapps.ksiegozbiorsb3.model.Category;

@Component
public class CategoryCommandToCategory implements Converter<CategoryCommand, Category> {

    @Synchronized
    @Nullable
    @Override
    public Category convert(CategoryCommand categoryCommand) {
        if (categoryCommand == null) {
            return null;
        } else {
            final Category category = new Category();
            category.setId(categoryCommand.getId());
            category.setName(categoryCommand.getName());
            return category;
        }
    }
}
