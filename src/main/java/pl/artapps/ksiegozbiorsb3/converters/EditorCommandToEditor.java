package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.EditorCommand;
import pl.artapps.ksiegozbiorsb3.model.Editor;

@Component
public class EditorCommandToEditor implements Converter<EditorCommand, Editor> {

    @Synchronized
    @Nullable
    @Override
    public Editor convert(EditorCommand editorCommand) {
        if (editorCommand == null) {
            return null;
        } else {
            final Editor editor = new Editor();
            if (editorCommand.getId() != null && !"".equals(editorCommand.getId())) {
                editor.setId(new Long(editorCommand.getId()));
            }
            editor.setName(editorCommand.getName());
            editor.setCity(editorCommand.getCity());
            return editor;
        }
    }
}
