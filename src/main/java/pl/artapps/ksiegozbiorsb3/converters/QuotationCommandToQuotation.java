package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.QuotationCommand;
import pl.artapps.ksiegozbiorsb3.model.Quotation;

@Component
public class QuotationCommandToQuotation implements Converter<QuotationCommand, Quotation> {

    @Synchronized
    @Nullable
    @Override
    public Quotation convert(QuotationCommand quotationCommand) {
        if (quotationCommand == null) {
            return null;
        } else {
            final Quotation quotation = new Quotation();
            quotation.setId(quotationCommand.getId());
            quotation.setName(quotationCommand.getName());
            return quotation;
        }
    }
}
