package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.AuthorCommand;
import pl.artapps.ksiegozbiorsb3.model.Author;

@Component
public class AuthorToAuthorCommand implements Converter<Author, AuthorCommand> {

    @Synchronized
    @Nullable
    @Override
    public AuthorCommand convert(Author author) {
        if (author == null) {
            return null;
        } else {
            final AuthorCommand authorCommand = new AuthorCommand();
            authorCommand.setId(author.getId());
            authorCommand.setFirstName(author.getFirstName());
            authorCommand.setLastName(author.getLastName());
            return authorCommand;
        }
    }
}
