package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.QuotationCommand;
import pl.artapps.ksiegozbiorsb3.model.Quotation;

@Component
public class QuotationToQuotationCommand implements Converter<Quotation, QuotationCommand> {

    @Synchronized
    @Nullable
    @Override
    public QuotationCommand convert(Quotation quotation) {
        if (quotation == null) {
            return null;
        } else {
            final QuotationCommand quotationCommand = new QuotationCommand();
            quotationCommand.setId(quotation.getId());
            quotationCommand.setName(quotation.getName());
            return quotationCommand;
        }
    }
}
