package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.IsbnCommand;
import pl.artapps.ksiegozbiorsb3.model.Isbn;

@Component
public class IsbnCommandToIsbn implements Converter<IsbnCommand, Isbn> {

    @Synchronized
    @Nullable
    @Override
    public Isbn convert(IsbnCommand isbnCommand) {
        if (isbnCommand == null) {
            return null;
        } else {
            final Isbn isbn = new Isbn();
            isbn.setId(isbnCommand.getId());
            isbn.setIsbnNumber(isbnCommand.getIsbnNumber());
            return isbn;
        }
    }
}
