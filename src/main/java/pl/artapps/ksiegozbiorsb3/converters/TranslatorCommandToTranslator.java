package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.TranslatorCommand;
import pl.artapps.ksiegozbiorsb3.model.Translator;

@Component
public class TranslatorCommandToTranslator implements Converter<TranslatorCommand, Translator> {

    @Synchronized
    @Nullable
    @Override
    public Translator convert(TranslatorCommand translatorCommand) {
        if (translatorCommand == null) {
            return null;
        } else {
            final Translator translator = new Translator();
            translator.setId(translatorCommand.getId());
            translator.setFirstName(translatorCommand.getFirstName());
            translator.setLastName(translatorCommand.getLastName());
            return translator;
        }
    }
}
