package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.LocationCommand;
import pl.artapps.ksiegozbiorsb3.model.Location;

@Component
public class LocationToLocationCommand implements Converter<Location, LocationCommand> {

    @Synchronized
    @Nullable
    @Override
    public LocationCommand convert(Location location) {
        if (location == null) {
            return null;
        } else {
            final LocationCommand locationCommand = new LocationCommand();
            locationCommand.setId(location.getId());
            locationCommand.setName(location.getName());
            return locationCommand;
        }
    }
}
