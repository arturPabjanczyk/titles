package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.SectionCommand;
import pl.artapps.ksiegozbiorsb3.model.Section;

@Component
public class SectionToSectionCommand implements Converter<Section, SectionCommand> {

    @Synchronized
    @Nullable
    @Override
    public SectionCommand convert(Section section) {
        if (section == null) {
            return null;
        } else {
            final SectionCommand sectionCommand = new SectionCommand();
            sectionCommand.setId(section.getId());
            sectionCommand.setName(section.getName());
            return sectionCommand;
        }
    }
}
