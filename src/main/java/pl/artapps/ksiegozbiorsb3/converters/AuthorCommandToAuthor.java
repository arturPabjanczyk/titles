package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.AuthorCommand;
import pl.artapps.ksiegozbiorsb3.model.Author;

@Component
public class AuthorCommandToAuthor implements Converter<AuthorCommand, Author> {

    @Synchronized
    @Nullable
    @Override
    public Author convert(AuthorCommand authorCommand) {
        if (authorCommand == null) {
            return null;
        } else {
            final Author author = new Author();
            author.setId(authorCommand.getId());
            author.setFirstName(authorCommand.getFirstName());
            author.setLastName(authorCommand.getLastName());
            return author;
        }
    }
}
