package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.TranslatorCommand;
import pl.artapps.ksiegozbiorsb3.model.Translator;

@Component
public class TranslatorToTranslatorCommand implements Converter<Translator, TranslatorCommand> {

    @Synchronized
    @Nullable
    @Override
    public TranslatorCommand convert(Translator translator) {
        if (translator == null) {
            return null;
        } else {
            final TranslatorCommand translatorCommand = new TranslatorCommand();
            translatorCommand.setId(translator.getId());
            translatorCommand.setFirstName(translator.getFirstName());
            translatorCommand.setLastName(translator.getLastName());
            return translatorCommand;
        }
    }
}
