package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.TagCommand;
import pl.artapps.ksiegozbiorsb3.model.Tag;

@Component
public class TagCommandToTag implements Converter<TagCommand, Tag> {

    @Synchronized
    @Nullable
    @Override
    public Tag convert(TagCommand tagCommand) {
        if (tagCommand == null) {
            return null;
        } else {
            final Tag tag = new Tag();
            tag.setId(tagCommand.getId());
            tag.setName(tagCommand.getName());
            return tag;
        }
    }
}
