package pl.artapps.ksiegozbiorsb3.converters;

import lombok.Synchronized;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import pl.artapps.ksiegozbiorsb3.commands.IsbnCommand;
import pl.artapps.ksiegozbiorsb3.model.Isbn;

@Component
public class IsbnToIsbnCommand implements Converter<Isbn, IsbnCommand> {

    @Synchronized
    @Nullable
    @Override
    public IsbnCommand convert(Isbn isbn) {
        if (isbn == null) {
            return null;
        } else {
            final IsbnCommand isbnCommand = new IsbnCommand();
            isbnCommand.setId(isbn.getId());
            isbnCommand.setIsbnNumber(isbn.getIsbnNumber());
            return isbnCommand;
        }
    }
}
