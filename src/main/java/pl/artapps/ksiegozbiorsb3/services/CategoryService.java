package pl.artapps.ksiegozbiorsb3.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.artapps.ksiegozbiorsb3.commands.CategoryCommand;
import pl.artapps.ksiegozbiorsb3.converters.CategoryCommandToCategory;
import pl.artapps.ksiegozbiorsb3.converters.CategoryToCategoryCommand;
import pl.artapps.ksiegozbiorsb3.model.Category;
import pl.artapps.ksiegozbiorsb3.repositories.CategoryRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final CategoryCommandToCategory categoryCommandToCategory;
    private final CategoryToCategoryCommand categoryToCategoryCommand;

    public CategoryService(CategoryRepository categoryRepository,
                               CategoryCommandToCategory categoryCommandToCategory,
                               CategoryToCategoryCommand categoryToCategoryCommand) {
        this.categoryRepository = categoryRepository;
        this.categoryCommandToCategory = categoryCommandToCategory;
        this.categoryToCategoryCommand = categoryToCategoryCommand;
    }

    
    public List<Category> findAll() {
        log.debug(" ...in CategoryService...");
        List<Category> categoryList = new LinkedList<>();
        categoryRepository.findAll().iterator().forEachRemaining(categoryList::add);
        return categoryList;
    }

    
    public Category findById(Long id) {

        Optional<Category> categoryOptional = categoryRepository.findById(id);

        if (!categoryOptional.isPresent()) {
            throw new RuntimeException("Kategoria nie odnaleziona");
        }
        return categoryOptional.get();
    }

    
    public CategoryCommand saveCategoryCommand(CategoryCommand command) {
        Category detachedCategory = categoryCommandToCategory.convert(command);

        Category savedCategory = categoryRepository.save(detachedCategory);
        log.debug("Zapisano CategoryId:" + savedCategory.getId());
        return categoryToCategoryCommand.convert(savedCategory);
    }

    
    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    
    public void delete(Category category) {
        categoryRepository.delete(category);
    }

    
    public void deleteById(Long id) {
        categoryRepository.deleteById(id);
    }

    
    public List<Category> searchCategoriesContainingString(String searchText) {
        return categoryRepository.findByNameContains(searchText);
    }

    
    public Category findByName(String name) {
        Optional<Category> categoryOptional = categoryRepository.findByName(name);

        if (!categoryOptional.isPresent()) {
            throw new RuntimeException("Kategoria nie odnaleziona");
        }
        return categoryOptional.get();
    }
}
