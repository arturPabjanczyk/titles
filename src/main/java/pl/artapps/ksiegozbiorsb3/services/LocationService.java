package pl.artapps.ksiegozbiorsb3.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.artapps.ksiegozbiorsb3.commands.LocationCommand;
import pl.artapps.ksiegozbiorsb3.converters.LocationCommandToLocation;
import pl.artapps.ksiegozbiorsb3.converters.LocationToLocationCommand;
import pl.artapps.ksiegozbiorsb3.model.Location;
import pl.artapps.ksiegozbiorsb3.repositories.LocationRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class LocationService {

    private final LocationRepository locationRepository;
    private final LocationCommandToLocation locationCommandToLocation;
    private final LocationToLocationCommand locationToLocationCommand;

    public LocationService(LocationRepository locationRepository,
                               LocationCommandToLocation locationCommandToLocation,
                               LocationToLocationCommand locationToLocationCommand) {
        this.locationRepository = locationRepository;
        this.locationCommandToLocation = locationCommandToLocation;
        this.locationToLocationCommand = locationToLocationCommand;
    }

    
    public List<Location> findAll() {
        log.debug(" ...in LocationService...");
        List<Location> locationList = new LinkedList<>();
        locationRepository.findAll().iterator().forEachRemaining(locationList::add);
        return locationList;
    }

    
    public Location findById(Long id) {

        Optional<Location> locationOptional = locationRepository.findById(id);

        if (!locationOptional.isPresent()) {
            throw new RuntimeException("Lokalizacja nie odnaleziona");
        }
        return locationOptional.get();
    }

    
    public LocationCommand saveLocationCommand(LocationCommand command) {
        Location detachedLocation = locationCommandToLocation.convert(command);

        Location savedLocation = locationRepository.save(detachedLocation);
        log.debug("Zapisano LocationId:" + savedLocation.getId());
        return locationToLocationCommand.convert(savedLocation);
    }

    
    public Location save(Location location) {
        return locationRepository.save(location);
    }

    
    public void delete(Location location) {
        locationRepository.delete(location);
    }

    
    public void deleteById(Long id) {
        locationRepository.deleteById(id);
    }

    
    public Location findByName(String name) {
        Optional<Location> locationOptional = locationRepository.findByName(name);

        if (!locationOptional.isPresent()) {
            throw new RuntimeException("Lokalizacja nie odnaleziona");
        }
        return locationOptional.get();
    }
}
