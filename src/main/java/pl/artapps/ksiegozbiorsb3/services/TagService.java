package pl.artapps.ksiegozbiorsb3.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.artapps.ksiegozbiorsb3.commands.TagCommand;
import pl.artapps.ksiegozbiorsb3.converters.TagCommandToTag;
import pl.artapps.ksiegozbiorsb3.converters.TagToTagCommand;
import pl.artapps.ksiegozbiorsb3.model.Tag;
import pl.artapps.ksiegozbiorsb3.repositories.TagRepository;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class TagService {

    private final TagRepository tagRepository;
    private final TagCommandToTag tagCommandToTag;
    private final TagToTagCommand tagToTagCommand;

    public TagService(TagRepository tagRepository,
                          TagCommandToTag tagCommandToTag,
                          TagToTagCommand tagToTagCommand) {
        this.tagRepository = tagRepository;
        this.tagCommandToTag = tagCommandToTag;
        this.tagToTagCommand = tagToTagCommand;
    }

    
    public List<Tag> findAll() {
        log.debug(" ...in TagService...");
        List<Tag> tagList = tagRepository.findAll();
        tagList.sort(Comparator.comparing(Tag::getName, String::compareToIgnoreCase));
        return tagList;
    }

    
    public Tag findById(Long id) {

        Optional<Tag> tagOptional = tagRepository.findById(id);

        if (!tagOptional.isPresent()) {
            throw new RuntimeException("Tag nie odnaleziony");
        }
        return tagOptional.get();
    }

    
    public TagCommand saveTagCommand(TagCommand command) {
        Tag detachedTag = tagCommandToTag.convert(command);

        Tag savedTag = tagRepository.save(detachedTag);
        log.debug("Zapisano TagId:" + savedTag.getId());
        return tagToTagCommand.convert(savedTag);
    }

    
    public Tag save(Tag tag) {
        return tagRepository.save(tag);
    }

    
    public void delete(Tag tag) {
        tagRepository.delete(tag);
    }

    
    public void deleteById(Long id) {
        tagRepository.deleteById(id);
    }

    
    public List<Tag> searchSectionsContainingString(String searchText) {
        return tagRepository.findByNameContains(searchText);
    }

    
    public Tag findByName(String name) {
        Optional<Tag> tagOptional = tagRepository.findByName(name);

        if (!tagOptional.isPresent()) {
            throw new RuntimeException("Tag nie odnaleziony");
        }
        return tagOptional.get();
    }
}
