package pl.artapps.ksiegozbiorsb3.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.artapps.ksiegozbiorsb3.commands.TranslatorCommand;
import pl.artapps.ksiegozbiorsb3.converters.TranslatorCommandToTranslator;
import pl.artapps.ksiegozbiorsb3.converters.TranslatorToTranslatorCommand;
import pl.artapps.ksiegozbiorsb3.model.Translator;
import pl.artapps.ksiegozbiorsb3.repositories.TranslatorRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class TranslatorService {

    private final TranslatorRepository translatorRepository;
    private final TranslatorCommandToTranslator translatorCommandToTranslator;
    private final TranslatorToTranslatorCommand translatorToTranslatorCommand;

    public TranslatorService(TranslatorRepository translatorRepository,
                                 TranslatorCommandToTranslator translatorCommandToTranslator,
                                 TranslatorToTranslatorCommand translatorToTranslatorCommand) {
        this.translatorRepository = translatorRepository;
        this.translatorCommandToTranslator = translatorCommandToTranslator;
        this.translatorToTranslatorCommand = translatorToTranslatorCommand;
    }

    
    public List<Translator> findAll() {
        log.debug(" ...in TranslatorService...");
        List<Translator> translatorList = new LinkedList<>();
        translatorRepository.findAll().iterator().forEachRemaining(translatorList::add);
        return translatorList;
    }

    
    public Translator findById(Long id) {

        Optional<Translator> translatorOptional = translatorRepository.findById(id);

        if (!translatorOptional.isPresent()) {
            throw new RuntimeException("Tłumacz nie odnaleziony");
        }
        return translatorOptional.get();
    }

    
    public TranslatorCommand saveTranslatorCommand(TranslatorCommand command) {
        Translator detachedTranslator = translatorCommandToTranslator.convert(command);

        Translator savedTranslator = translatorRepository.save(detachedTranslator);
        log.debug("Zapisano TranslatorId:" + savedTranslator.getId());
        return translatorToTranslatorCommand.convert(savedTranslator);
    }

    
    public Translator save(Translator translator) {
        return translatorRepository.save(translator);
    }

    
    public void delete(Translator translator) {
        translatorRepository.delete(translator);
    }

    
    public void deleteById(Long id) {
        translatorRepository.deleteById(id);
    }

    
    public Translator findByLastName(String translatorName) {
        Optional<Translator> translatorOptional = translatorRepository.findByLastName(translatorName);

        if (!translatorOptional.isPresent()) {
            throw new RuntimeException("Tłumacz nie odnaleziony");
        }
        return translatorOptional.get();
    }
}
