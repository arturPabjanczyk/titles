package pl.artapps.ksiegozbiorsb3.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.artapps.ksiegozbiorsb3.model.Isbn;
import pl.artapps.ksiegozbiorsb3.repositories.IsbnRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class IsbnService {

    private final IsbnRepository isbnRepository;

    public IsbnService(IsbnRepository isbnRepository) {
        this.isbnRepository = isbnRepository;
    }

    
    public List<Isbn> findAll() {
        log.debug(" ...in IsbnService...");
        List<Isbn> isbnList = new LinkedList<>();
        isbnRepository.findAll().iterator().forEachRemaining(isbnList::add);
        return isbnList;
    }

    
    public Isbn findByIsbnNumber(String isbnNumber) {

        Optional<Isbn> isbnOptional = isbnRepository.findByIsbnNumber(isbnNumber);

        if (!isbnOptional.isPresent()) {
            throw new RuntimeException("Isbn nie odnaleziony");
        }
        return isbnOptional.get();
    }

    
    public Isbn save(Isbn isbn) {
        return isbnRepository.save(isbn);
    }
}
