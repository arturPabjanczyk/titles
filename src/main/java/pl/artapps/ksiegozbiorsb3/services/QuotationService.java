package pl.artapps.ksiegozbiorsb3.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.artapps.ksiegozbiorsb3.commands.QuotationCommand;
import pl.artapps.ksiegozbiorsb3.converters.QuotationCommandToQuotation;
import pl.artapps.ksiegozbiorsb3.converters.QuotationToQuotationCommand;
import pl.artapps.ksiegozbiorsb3.model.Quotation;
import pl.artapps.ksiegozbiorsb3.repositories.QuotationRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class QuotationService {

    private final QuotationRepository quotationRepository;
    private final QuotationCommandToQuotation quotationCommandToQuotation;
    private final QuotationToQuotationCommand quotationToQuotationCommand;

    public QuotationService(QuotationRepository quotationRepository,
                                QuotationCommandToQuotation quotationCommandToQuotation,
                                QuotationToQuotationCommand quotationToQuotationCommand) {
        this.quotationRepository = quotationRepository;
        this.quotationCommandToQuotation = quotationCommandToQuotation;
        this.quotationToQuotationCommand = quotationToQuotationCommand;
    }


    
    public List<Quotation> findAll() {
        log.debug(" ...in QuotationService...");
        List<Quotation> quotationList = new LinkedList<>();
        quotationRepository.findAll().iterator().forEachRemaining(quotationList::add);
        return quotationList;
    }

    
    public Quotation findById(Long id) {

        Optional<Quotation> quotationOptional = quotationRepository.findById(id);

        if (!quotationOptional.isPresent()) {
            throw new RuntimeException("Cytat nie odnaleziony");
        }
        return quotationOptional.get();
    }

    
    public QuotationCommand saveQuotationCommand(QuotationCommand command) {
        Quotation detachedQuotation = quotationCommandToQuotation.convert(command);

        Quotation savedQuotation = quotationRepository.save(detachedQuotation);
        log.debug("Zapisano QuotationId:" + savedQuotation.getId());
        return quotationToQuotationCommand.convert(savedQuotation);
    }

    
    public Quotation save(Quotation quotation) {
        return quotationRepository.save(quotation);
    }

    
    public void delete(Quotation quotation) {
        quotationRepository.delete(quotation);
    }

    
    public void deleteById(Long id) {
        quotationRepository.deleteById(id);
    }

    
    public Quotation findByName(String name) {
        Optional<Quotation> quotationOptional = quotationRepository.findByName(name);

        if (!quotationOptional.isPresent()) {
            throw new RuntimeException("Cytat nie odnaleziony");
        }
        return quotationOptional.get();
    }
}