package pl.artapps.ksiegozbiorsb3.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.artapps.ksiegozbiorsb3.commands.FormatCommand;
import pl.artapps.ksiegozbiorsb3.converters.FormatCommandToFormat;
import pl.artapps.ksiegozbiorsb3.converters.FormatToFormatCommand;
import pl.artapps.ksiegozbiorsb3.model.Format;
import pl.artapps.ksiegozbiorsb3.repositories.FormatRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class FormatService {

    private final FormatRepository formatRepository;
    private final FormatCommandToFormat formatCommandToFormat;
    private final FormatToFormatCommand formatToFormatCommand;

    public FormatService(FormatRepository formatRepository,
                             FormatCommandToFormat formatCommandToFormat,
                             FormatToFormatCommand formatToFormatCommand) {
        this.formatRepository = formatRepository;
        this.formatCommandToFormat = formatCommandToFormat;
        this.formatToFormatCommand = formatToFormatCommand;
    }

    
    public List<Format> findAll() {
        log.debug(" ...in FormatService...");
        List<Format> formatList = new LinkedList<>();
        formatRepository.findAll().iterator().forEachRemaining(formatList::add);
        return formatList;
    }

    
    public Format findById(Long id) {

        Optional<Format> formatOptional = formatRepository.findById(id);

        if (!formatOptional.isPresent()) {
            throw new RuntimeException("Format nie odnaleziony");
        }
        return formatOptional.get();
    }

    
    public FormatCommand saveFormatCommand(FormatCommand command) {
        Format detachedFormat = formatCommandToFormat.convert(command);

        Format savedFormat = formatRepository.save(detachedFormat);
        log.debug("Zapisano FormatId:" + savedFormat.getId());
        return formatToFormatCommand.convert(savedFormat);
    }

    
    public Format save(Format format) {
        return formatRepository.save(format);
    }

    
    public void delete(Format format) {
        formatRepository.delete(format);
    }

    
    public void deleteById(Long id) {
        formatRepository.deleteById(id);
    }

    
    public Format findByname(String name) {
        Optional<Format> formatOptional = formatRepository.findByname(name);

        if (!formatOptional.isPresent()) {
            throw new RuntimeException("Format nie odnaleziony");
        }
        return formatOptional.get();
    }
}
