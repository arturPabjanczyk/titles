package pl.artapps.ksiegozbiorsb3.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.artapps.ksiegozbiorsb3.commands.AuthorCommand;
import pl.artapps.ksiegozbiorsb3.converters.AuthorCommandToAuthor;
import pl.artapps.ksiegozbiorsb3.converters.AuthorToAuthorCommand;
import pl.artapps.ksiegozbiorsb3.model.Author;
import pl.artapps.ksiegozbiorsb3.repositories.AuthorRepository;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class AuthorService {

    private final AuthorRepository authorRepository;
    private final AuthorCommandToAuthor authorCommandToAuthor;
    private final AuthorToAuthorCommand authorToAuthorCommand;

    public AuthorService(AuthorRepository authorRepository, AuthorCommandToAuthor authorCommandToAuthor,
                         AuthorToAuthorCommand authorToAuthorCommand) {
        this.authorRepository = authorRepository;
        this.authorCommandToAuthor = authorCommandToAuthor;
        this.authorToAuthorCommand = authorToAuthorCommand;
    }

    public List<Author> findAll() {
        log.debug(" ...in AuthorService...");
        List<Author> authorList = new LinkedList<>();
        authorRepository.findAll().iterator().forEachRemaining(authorList::add);
        authorList.sort(Comparator.comparing(Author::getLastName, String::compareToIgnoreCase));
        return authorList;
    }

    public Author findById(Long id) {
        Optional<Author> authorOptional = authorRepository.findById(id);

        if (!authorOptional.isPresent()) {
            throw new RuntimeException("Autor nie odnaleziony");
        }
        return authorOptional.get();
    }

    public Author findByLastName(String lastName) {
        Optional<Author> authorOptional = authorRepository.findByLastName(lastName);

        if (!authorOptional.isPresent()) {
            throw new RuntimeException("Autor nie odnaleziony");
        }
        return authorOptional.get();
    }

    public Author findByName(String firstName, String lastName) {
        Optional<Author> authorOptional = authorRepository.findByFirstNameAndLastName(firstName, lastName);

        if (!authorOptional.isPresent()) {
            throw new RuntimeException("Autor nie odnaleziony");
        }
        return authorOptional.get();
    }

    public List<Author> findByNameChunk(String nameChunk) {
        List<Author> authorListWithFirstNameChunks = authorRepository.findByFirstNameContains(nameChunk);
        List<Author> authorListWithLastNameChunks = authorRepository.findByLastNameContains(nameChunk);
        authorListWithFirstNameChunks.addAll(authorListWithLastNameChunks);
        return authorListWithFirstNameChunks;
    }

    @Transactional
    public AuthorCommand saveAuthorCommand(AuthorCommand command) {
        Author detachedAuthor = authorCommandToAuthor.convert(command);

        Author savedAuthor = authorRepository.save(detachedAuthor);
        log.debug("Zapisano AuthorId:" + savedAuthor.getId());
        return authorToAuthorCommand.convert(savedAuthor);
    }

    public Author save(Author author) {
        return authorRepository.save(author);
    }

    public void delete(Author author) {
        authorRepository.delete(author);
    }

    public void deleteById(Long id) {
        authorRepository.deleteById(id);
    }

    public List<Author> searchAuthorsWhoseFirstNameContainsString(String searchText) {
        return authorRepository.findByFirstNameContains(searchText);
    }

    public List<Author> searchAuthorsWhoseLastNameContainsString(String searchText) {
        return authorRepository.findByLastNameContains(searchText);
    }
}
