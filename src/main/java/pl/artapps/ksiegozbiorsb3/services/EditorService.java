package pl.artapps.ksiegozbiorsb3.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.artapps.ksiegozbiorsb3.commands.EditorCommand;
import pl.artapps.ksiegozbiorsb3.converters.EditorCommandToEditor;
import pl.artapps.ksiegozbiorsb3.converters.EditorToEditorCommand;
import pl.artapps.ksiegozbiorsb3.model.Editor;
import pl.artapps.ksiegozbiorsb3.repositories.EditorRepository;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class EditorService {

    private final EditorRepository editorRepository;
    private final EditorCommandToEditor editorCommandToEditor;
    private final EditorToEditorCommand editorToEditorCommand;

    public EditorService(EditorRepository editorRepository,
                             EditorCommandToEditor editorCommandToEditor,
                             EditorToEditorCommand editorToEditorCommand) {
        this.editorRepository = editorRepository;
        this.editorCommandToEditor = editorCommandToEditor;
        this.editorToEditorCommand = editorToEditorCommand;
    }

    
    public List<Editor> findAll() {
        log.debug(" ...in EditorService...");
        List<Editor> editorList = new LinkedList<>();
        editorRepository.findAll().iterator().forEachRemaining(editorList::add);
        editorList.sort(Comparator.comparing(Editor::getName, String::compareToIgnoreCase));
        return editorList;
    }

    
    public Editor findById(Long id) {

        Optional<Editor> editorOptional = editorRepository.findById(id);

        if (!editorOptional.isPresent()) {
            throw new RuntimeException("Wydawnictwo nie odnalezione");
        }
        return editorOptional.get();
    }

    
    public EditorCommand saveEditorCommand(EditorCommand command) {
        Editor detachedEditor = editorCommandToEditor.convert(command);

        Editor savedEditor = editorRepository.save(detachedEditor);
        log.debug("Zapisano EditorId:" + savedEditor.getId());
        return editorToEditorCommand.convert(savedEditor);
    }

    
    public Editor save(Editor editor) {
        return editorRepository.save(editor);
    }

    
    public void delete(Editor editor) {
        editorRepository.delete(editor);
    }

    
    public void deleteById(Long id) {
        editorRepository.deleteById(id);
    }

    
    public Editor findByName(String name) {
        Optional<Editor> editorOptional = editorRepository.findByName(name);

        if (!editorOptional.isPresent()) {
            throw new RuntimeException("Wydawnictwo nie odnalezione");
        }
        return editorOptional.get();
    }
}
