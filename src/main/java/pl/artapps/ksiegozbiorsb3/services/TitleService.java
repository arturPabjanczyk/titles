package pl.artapps.ksiegozbiorsb3.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.artapps.ksiegozbiorsb3.commands.TitleCommand;
import pl.artapps.ksiegozbiorsb3.converters.TitleCommandToTitle;
import pl.artapps.ksiegozbiorsb3.converters.TitleToTitleCommand;
import pl.artapps.ksiegozbiorsb3.model.Author;
import pl.artapps.ksiegozbiorsb3.model.Tag;
import pl.artapps.ksiegozbiorsb3.model.Title;
import pl.artapps.ksiegozbiorsb3.repositories.TitleRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Slf4j
@Service
public class TitleService {

    private final TitleRepository titleRepository;
    private final TitleCommandToTitle titleCommandToTitle;
    private final TitleToTitleCommand titleToTitleCommand;
    private final AuthorService authorService;
    private final TagService tagService;


    public TitleService(
            TitleRepository titleRepository,
            AuthorService authorService,
            TitleCommandToTitle titleCommandToTitle,
            TitleToTitleCommand titleToTitleCommand, TagService tagService) {
        this.titleRepository = titleRepository;
        this.authorService = authorService;
        this.titleCommandToTitle = titleCommandToTitle;
        this.titleToTitleCommand = titleToTitleCommand;
        this.tagService = tagService;
    }

    public List<Title> findAll() {
        List<Title> titleList = titleRepository.findAll();
        titleList.sort(Comparator.comparing(Title::getName, String::compareToIgnoreCase));
        return titleList;
    }

    public Long findFirstFreeCatalogNumber() {
        List<Title> titleList = titleRepository.findAll();
        Set<Long> catNumberSet = new HashSet<>();
        titleList.forEach(title -> {
            if (title.getCatNumber() != null) {
                catNumberSet.add(title.getCatNumber());
            }
        });
        List<Long> catNumberList = new ArrayList<>(catNumberSet);
        Collections.sort(catNumberList);
        Long maxCatNumber = Collections.max(catNumberList);
        for (long i = 1; i < maxCatNumber; i++) {
            if (!catNumberList.contains(i)) return i;
        }
        return maxCatNumber + 1;
    }

    public Title findById(Long id) {
        Optional<Title> titleOptional = titleRepository.findById(id);
        if (!titleOptional.isPresent()) {
            throw new RuntimeException("Tytuł nie odnaleziony");
        }
        return titleOptional.get();
    }

    public Title findByTitleAndAuthorId(String title, Long authorId) {
        List<Title> titleList = titleRepository.findByName(title);
        Author author = authorService.findById(authorId);
        for (Title t : titleList) {
            if (t.getAuthors().contains(author)) return t;
        }
        return null;
    }

    public Title findByTitleAndTagId(String title, Long tagId) {
        List<Title> titleList = titleRepository.findByName(title);
        Tag tag = tagService.findById(tagId);
        for (Title t : titleList) {
            if (t.getTags().contains(tag)) return t;
        }
        return null;
    }

    public List<Title> findTitlesBySectionId(Long id) {
        return titleRepository.findBySectionId(id);
    }

    public List<Title> findTitlesByEditorId(Long id) {
        return titleRepository.findByEditorId(id);
    }

    @Transactional
    public TitleCommand saveTitleCommand(TitleCommand command) {
        Title detachedTitle = titleCommandToTitle.convert(command);
        Title savedTitle = titleRepository.save(detachedTitle);
        log.debug("Zapisano TitleId:" + savedTitle.getId());
        return titleToTitleCommand.convert(savedTitle);
    }

    public Title save(Title title) {
        return titleRepository.save(title);
    }

    public void delete(Title title) {
        titleRepository.delete(title);
    }

    public void deleteById(Long id) {
        titleRepository.deleteById(id);
    }

    public List<Title> searchTitlesContainingString(String searchText) {
        return titleRepository.findByNameContains(searchText);
    }

    public List<Title> findTitlesByTagId(Long id) {
        Tag tag = tagService.findById(id);
        return titleRepository.findByTagsContaining(tag);
    }

    public void moveAuthors() {
//        int counter = 0;
//        List<Title> titleList = titleRepository.findAll();
//        for (Title title1 : titleList) {
//            Section section = new Section();
//            Section section = new Section();
//            if (!(title1.getCategories().isEmpty())) {
//                List<Section> sectionList = new ArrayList<>(title1.getCategories());
//                if (sectionList.get(0) != null) section = sectionList.get(0);
//                section.setId(section.getId());
//                section.setName(section.getName());
//                section.setTitles(section.getTitles());
//            } else {
//                section.setId(54L);
//                section.setName("temporary_cat");
//            }
//            System.out.println(counter++ + " " + title1.getId() + " " + section.getName() + " " + section.getName());
//            title1.setSection(section);
//            titleRepository.save(title1);
//        }
//        System.out.println(counter);
    }

    public List<Title> findByAuthor(Author author) {
        return titleRepository.findTitleByAuthorsContains(author);
    }
}