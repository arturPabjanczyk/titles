package pl.artapps.ksiegozbiorsb3.services;

import lombok.extern.slf4j.*;
import org.springframework.stereotype.*;
import pl.artapps.ksiegozbiorsb3.commands.*;
import pl.artapps.ksiegozbiorsb3.converters.*;
import pl.artapps.ksiegozbiorsb3.model.*;
import pl.artapps.ksiegozbiorsb3.repositories.*;

import java.util.*;

@Slf4j
@Service
public class SectionService {

    private final SectionRepository sectionRepository;
    private final SectionCommandToSection sectionCommandToSection;
    private final SectionToSectionCommand sectionToSectionCommand;

    public SectionService(SectionRepository sectionRepository,
                              SectionCommandToSection sectionCommandToSection,
                              SectionToSectionCommand sectionToSectionCommand) {
        this.sectionRepository = sectionRepository;
        this.sectionCommandToSection = sectionCommandToSection;
        this.sectionToSectionCommand = sectionToSectionCommand;
    }

    public List<Section> findAll() {
        log.debug(" ...in SectionService...");
        List<Section> sectionList = sectionRepository.findAll();
        sectionList.sort(Comparator.comparing(Section::getName, String::compareToIgnoreCase));
        return sectionList;
    }

    public Section findById(Long id) {

        Optional<Section> sectionOptional = sectionRepository.findById(id);

        if (!sectionOptional.isPresent()) {
            throw new RuntimeException("Kategoria nie odnaleziona");
        }
        return sectionOptional.get();
    }

    public SectionCommand saveSectionCommand(SectionCommand command) {
        Section detachedSection = sectionCommandToSection.convert(command);

        Section savedSection = sectionRepository.save(detachedSection);
        log.debug("Zapisano SectionId:" + savedSection.getId());
        return sectionToSectionCommand.convert(savedSection);
    }

    public Section save(Section section) {
        return sectionRepository.save(section);
    }

    public void delete(Section section) {
        sectionRepository.delete(section);
    }

    public void deleteById(Long id) {
        sectionRepository.deleteById(id);
    }

    public List<Section> searchSectionsContainingString(String searchText) {
        return sectionRepository.findByNameContains(searchText);
    }

    public Section findByName(String name) {
        Optional<Section> sectionOptional = sectionRepository.findByName(name);

        if (!sectionOptional.isPresent()) {
            throw new RuntimeException("Kategoria nie odnaleziona");
        }
        return sectionOptional.get();
    }
}