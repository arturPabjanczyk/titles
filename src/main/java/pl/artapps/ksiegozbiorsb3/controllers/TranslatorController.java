package pl.artapps.ksiegozbiorsb3.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.artapps.ksiegozbiorsb3.repositories.TranslatorRepository;
import pl.artapps.ksiegozbiorsb3.services.TranslatorService;

/**
 * created by ap on 22/1/19
 */

@RequestMapping("/translators")
@Controller
public class TranslatorController {

    private TranslatorRepository translatorRepository;
    private TranslatorService translatorService;

    public TranslatorController(TranslatorRepository translatorRepository, TranslatorService translatorService) {
        this.translatorRepository = translatorRepository;
        this.translatorService = translatorService;
    }

    @RequestMapping("/map")
    public String getTranslatrsMap(Model model) {
        model.addAttribute("translators", translatorService.findAll());
        return "translators/map";
    }

    @RequestMapping({"", "/", "index", "index.html"})
    public String getTranslators(Model model) {
        model.addAttribute("translators", translatorRepository.findAll());
        return "translators/index";
    }
}
