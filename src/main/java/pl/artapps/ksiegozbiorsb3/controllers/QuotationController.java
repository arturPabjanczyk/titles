package pl.artapps.ksiegozbiorsb3.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.artapps.ksiegozbiorsb3.repositories.QuotationRepository;
import pl.artapps.ksiegozbiorsb3.services.QuotationService;

/**
 * created by ap on 12/1/19
 */

@RequestMapping("/quotations")
@Controller
public class QuotationController {

    private QuotationRepository quotationRepository;
    private QuotationService quotationService;

    public QuotationController(QuotationRepository quotationRepository, QuotationService quotationService) {
        this.quotationRepository = quotationRepository;
        this.quotationService = quotationService;
    }

    @RequestMapping("/map")
    public String getQuotationsMap(Model model) {
        model.addAttribute("quotations", quotationService.findAll());
        return "quotations/map";
    }

    @RequestMapping({"", "/", "index", "index.html"})
    public String getQuotations(Model model) {
        model.addAttribute("quotations", quotationRepository.findAll());
        return "quotations/index";
    }
}
