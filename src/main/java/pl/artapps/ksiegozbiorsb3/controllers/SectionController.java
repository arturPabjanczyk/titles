package pl.artapps.ksiegozbiorsb3.controllers;

import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import pl.artapps.ksiegozbiorsb3.commands.*;
import pl.artapps.ksiegozbiorsb3.model.*;
import pl.artapps.ksiegozbiorsb3.services.*;

import java.util.*;

/**
 * created by ap on 25/4/19
 */

@RequestMapping("/sections")
@Controller
public class SectionController {

    private final SectionService sectionService;
    private final TitleService titleService;

    public SectionController(SectionService sectionService, TitleService titleService) {
        this.sectionService = sectionService;
        this.titleService = titleService;
    }

    @GetMapping({"", "/", "index", "index.html"})
    public String searchSection(Model model) {
        List<Section> sectionList = sectionService.findAll();
        sectionList.sort(Comparator.comparing(Section::getName, String::compareToIgnoreCase));
        model.addAttribute("sections", sectionList);
        model.addAttribute("searchField", new SearchField());
        return "sections/index";
    }

    @PostMapping("/search-by-string")
    public String searchSectionByString(@ModelAttribute SearchField searchField, Model model) {
        String searchText = searchField.getSearchText();
        List<Section> sectionList = sectionService.searchSectionsContainingString(searchText);
        sectionList.sort(Comparator.comparing(Section::getName, String::compareToIgnoreCase));
        model.addAttribute("sections", sectionList);
        Section section = new Section();
        section.setName(searchText);
        model.addAttribute("section", section);
        return "sections/found";
    }

    @PostMapping("/add-new-section")
    public String saveOrUpdate(@ModelAttribute SectionCommand command, Model model) {
        SectionCommand savedCommand = sectionService.saveSectionCommand(command);
        return "redirect:/sections/show/" + savedCommand.getId();
    }

    @GetMapping("/show/{id}")
    public String showSectionById(@PathVariable Long id, Model model) {
        Section section = sectionService.findById(id);
        model.addAttribute("section", section);
        List<Title> titleList = titleService.findTitlesBySectionId(id);
        titleList.sort(Comparator.comparing(Title::getName, String::compareToIgnoreCase));
        model.addAttribute("titles", titleList);
        return "sections/show";
    }
}
