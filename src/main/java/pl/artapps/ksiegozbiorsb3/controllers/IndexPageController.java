package pl.artapps.ksiegozbiorsb3.controllers;

import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

@Controller
public class IndexPageController {

    @GetMapping({"", "/", "index", "index.html", "index.htm"})
    public String indexPage() {
        return "index";
    }
}
