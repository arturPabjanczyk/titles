package pl.artapps.ksiegozbiorsb3.controllers;

import lombok.*;
import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import pl.artapps.ksiegozbiorsb3.model.*;
import pl.artapps.ksiegozbiorsb3.services.*;

import java.util.*;

/**
 * created by ap on 12/1/19
 */

@RequestMapping("/editors")
@Controller
@RequiredArgsConstructor
public class EditorController {

    private final EditorService editorService;
    private final TitleService titleService;

    @RequestMapping({"", "/", "index", "index.html"})
    public String getEditors(Model model) {
        model.addAttribute("editors", editorService.findAll());
        return "editors/index";
    }

    @GetMapping("/show/{id}")
    public String showEditorById(@PathVariable Long id, Model model) {
        Editor editor = editorService.findById(id);
        model.addAttribute("editor", editor);
        List<Title> titleList = titleService.findTitlesByEditorId(id);
        titleList.sort(Comparator.comparing(Title::getName, String::compareToIgnoreCase));
        model.addAttribute("titles", titleList);
        return "editors/show";
    }
}
