package pl.artapps.ksiegozbiorsb3.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.artapps.ksiegozbiorsb3.repositories.FormatRepository;
import pl.artapps.ksiegozbiorsb3.services.FormatService;

/**
 * created by ap on 12/1/19
 */

@RequestMapping("/formats")
@Controller
public class FormatController {

    private FormatRepository formatRepository;
    private FormatService formatService;

    public FormatController(FormatRepository formatRepository, FormatService formatService) {
        this.formatRepository = formatRepository;
        this.formatService = formatService;
    }

    @RequestMapping("/map")
    public String getFormatsMap(Model model) {
        model.addAttribute("formats", formatService.findAll());
        return "formats/map";
    }

    @RequestMapping({"", "/", "index", "index.html"})
    public String getFormats(Model model) {
        model.addAttribute("formats", formatRepository.findAll());
        return "formats/index";
    }
}
