package pl.artapps.ksiegozbiorsb3.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.artapps.ksiegozbiorsb3.commands.AuthorCommand;
import pl.artapps.ksiegozbiorsb3.commands.SearchField;
import pl.artapps.ksiegozbiorsb3.converters.AuthorToAuthorCommand;
import pl.artapps.ksiegozbiorsb3.model.Author;
import pl.artapps.ksiegozbiorsb3.model.Title;
import pl.artapps.ksiegozbiorsb3.services.AuthorService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * created by ap on 12/1/19
 */

@RequestMapping("/authors")
@Controller
public class AuthorController {

    private final AuthorService authorService;
    private final AuthorToAuthorCommand authorToAuthorCommand;

    public AuthorController(AuthorService authorService, AuthorToAuthorCommand authorToAuthorCommand) {
        this.authorService = authorService;
        this.authorToAuthorCommand = authorToAuthorCommand;
    }

    @RequestMapping({"", "/", "index", "index.html"})
    public String getAuthors(Model model) {
        model.addAttribute("authors", authorService.findAll());
        return "authors/index";
    }

    @GetMapping("/show/{id}")
    public String showAuthorById(@PathVariable Long id, Model model) {
        Author author = authorService.findById(id);
        model.addAttribute("author", author);
        List<Title> titleList = new ArrayList<>(author.getTitles());
        titleList.sort(Comparator.comparing(Title::getName, String::compareToIgnoreCase));
        model.addAttribute("titles", titleList);
        return "authors/show";
    }

    @GetMapping("/{name}")
    public String showAuthorByName(@PathVariable String name, Model model) {
        List<String> stringList = Arrays.asList(name.split(" "));
        int lastNameIndex = stringList.size() - 1;
        String lastName = stringList.get(lastNameIndex);
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < lastNameIndex; i++) {
            stringBuilder.append(stringList.get(i)).append(" ");
        }
        String firstName = stringBuilder.toString();
        if (!StringUtils.isEmpty(firstName)) {
            firstName = firstName.substring(0, firstName.length() - 1);
        }
        Author author = authorService.findByName(firstName, lastName);
        model.addAttribute("author", author);
        List<Title> titleList = new ArrayList<>(author.getTitles());
        titleList.sort(Comparator.comparing(Title::getName, String::compareToIgnoreCase));
        model.addAttribute("titles", titleList);
        return "authors/show";
    }

    @PostMapping("/update")
    public String updateAuthor(@RequestParam(value = "id", required = true) String id) {
        return "redirect:/authors/new-author/" + id;
    }

    @GetMapping("/new-author/{id}")
    public String updateAuthorId(@PathVariable String id, Model model) {
        Author author = authorService.findById(new Long(id));
        AuthorCommand authorCommand = authorToAuthorCommand.convert(author);
        model.addAttribute("author", authorCommand);
        return "authors/authorform";
    }

    @PostMapping("/add-new-author")
    public String saveOrUpdate(@ModelAttribute AuthorCommand command, Model model) {
        AuthorCommand savedCommand = authorService.saveAuthorCommand(command);
        model.addAttribute("title", new Title());
        return "redirect:/authors/show/" + savedCommand.getId();
    }

    @GetMapping("/search")
    public String searchAuthor(Model model) {
        model.addAttribute("searchField", new SearchField());
        return "authors/search";
    }

    @PostMapping("/search-by-string")
    public String searchAuthorByString(@ModelAttribute SearchField searchField, Model model) {
        String searchText = searchField.getSearchText();
        model.addAttribute("authorsWhoseFirstNameContainsString", authorService.searchAuthorsWhoseFirstNameContainsString(searchText));
        model.addAttribute("authorsWhoseLastNameContainsString", authorService.searchAuthorsWhoseLastNameContainsString(searchText));
        Author author = new Author();
        author.setLastName(searchText);
        model.addAttribute("author", author);
        return "authors/found";
    }
}
