package pl.artapps.ksiegozbiorsb3.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.artapps.ksiegozbiorsb3.repositories.LocationRepository;
import pl.artapps.ksiegozbiorsb3.services.LocationService;

/**
 * created by ap on 22/1/19
 */

@RequestMapping("/locations")
@Controller
public class LocationController {

    private LocationRepository locationRepository;
    private LocationService locationService;

    public LocationController(LocationRepository locationRepository, LocationService locationService) {
        this.locationRepository = locationRepository;
        this.locationService = locationService;
    }

    @RequestMapping("/map")
    public String getLocationsMap(Model model) {
        model.addAttribute("locations", locationService.findAll());
        return "locations/map";
    }

    @RequestMapping({"", "/", "index", "index.html"})
    public String getLocations(Model model) {
        model.addAttribute("locations", locationRepository.findAll());
        return "locations/index";
    }
}
