package pl.artapps.ksiegozbiorsb3.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.artapps.ksiegozbiorsb3.repositories.CategoryRepository;
import pl.artapps.ksiegozbiorsb3.services.CategoryService;

/**
 * created by ap on 12/1/19
 */

@RequestMapping("/categories")
@Controller
public class CategoryController {

    private CategoryRepository categoryRepository;
    private CategoryService categoryService;

    public CategoryController(CategoryRepository categoryRepository, CategoryService categoryService) {
        this.categoryRepository = categoryRepository;
        this.categoryService = categoryService;
    }

    @RequestMapping("/map")
    public String getCategoriesMap(Model model) {
        model.addAttribute("categories", categoryService.findAll());
        return "categories/map";
    }

    @RequestMapping({"", "/", "index", "index.html"})
    public String getCategories(Model model) {
        model.addAttribute("categories", categoryRepository.findAll());
        return "categories/index";
    }
}
