package pl.artapps.ksiegozbiorsb3.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import pl.artapps.ksiegozbiorsb3.commands.SearchField;
import pl.artapps.ksiegozbiorsb3.commands.TitleCommand;
import pl.artapps.ksiegozbiorsb3.converters.TitleToTitleCommand;
import pl.artapps.ksiegozbiorsb3.model.Title;
import pl.artapps.ksiegozbiorsb3.services.AuthorService;
import pl.artapps.ksiegozbiorsb3.services.EditorService;
import pl.artapps.ksiegozbiorsb3.services.IsbnService;
import pl.artapps.ksiegozbiorsb3.services.SectionService;
import pl.artapps.ksiegozbiorsb3.services.TagService;
import pl.artapps.ksiegozbiorsb3.services.TitleService;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

/**
 * created by ap on 12/1/19
 */

@Slf4j
@RequestMapping("/titles")
@Controller
public class TitleController {

    private static final String TITLES = "titles";
    private static final String TITLE = "title";
    private static final String TITLE_FORM = "titles/titleform";
    private static final String EDITORS = "editors";
    private static final String ISBNS = "isbns";
    private static final String TITLES_SHOW = "titles/show";
    private static final String TITLES_INDEX = "titles/index";
    private static final String TITLES_TITLE = "titles/title";
    private static final String AUTHORS = "authors";
    private static final String SECTIONS = "sections";
    private static final String TAGS = "tags";


    private final TitleService titleService;
    private final TitleToTitleCommand titleToTitleCommand;
    private final EditorService editorService;
    private final AuthorService authorService;
    private final IsbnService isbnService;
    private final SectionService sectionService;
    private final TagService tagService;

    public TitleController(TitleService titleService,
                           TitleToTitleCommand titleToTitleCommand,
                           EditorService editorService,
                           AuthorService authorService,
                           IsbnService isbnService, SectionService sectionService, TagService tagService) {
        this.titleService = titleService;
        this.titleToTitleCommand = titleToTitleCommand;
        this.editorService = editorService;
        this.authorService = authorService;
        this.isbnService = isbnService;
        this.sectionService = sectionService;
        this.tagService = tagService;
    }

    @GetMapping({"", "/", "index", "index.html"})
    public String getTitles(Model model) {
        List<Title> titleList = titleService.findAll();
        titleList.sort(Comparator.comparing(Title::getName, String::compareToIgnoreCase));
        model.addAttribute(TITLES, titleList);
        model.addAttribute("searchTitleField", new SearchField());
        return TITLES_INDEX;
    }

    @GetMapping(TITLE)
    public String getTitle(Model model) {
        model.addAttribute(TITLE, titleService.findById(1053L));
        return TITLES_TITLE;
    }

    @GetMapping("/find")
    public String findTitles(Model model) {
        return TITLES_INDEX;
    }

    @GetMapping("/show/{id}")
    public String showTitleById(@PathVariable String id, Model model) {
        model.addAttribute(TITLE, titleService.findById(Long.valueOf(id)));
        if ("1233".equals(id)) {
            try {
                throw new IllegalAccessException("22222222222222222222222222");
            } catch (IllegalAccessException e) {
                log.info("------------- " + e.getLocalizedMessage());
                model.addAttribute(TITLE, titleService.findById(Long.valueOf("1234")));
            }
        }

        return TITLES_SHOW;
    }

    @GetMapping("/{title}")
    public String showTitleById(
            @PathVariable String title,
            @RequestParam(value = "author_id", required = false) Long authorId,
            @RequestParam(value = "tag_id", required = false) Long tagId,
            Model model) {
        if (authorId != null) {
            model.addAttribute(TITLE, titleService.findByTitleAndAuthorId(title, authorId));
        } else if (tagId != null) {
            model.addAttribute(TITLE, titleService.findByTitleAndTagId(title, tagId));
        }
        return TITLES_SHOW;
    }

    @PostMapping("/update")
    public String updateTitle(@RequestParam(value = "id") String id) {
        return "redirect:/titles/new-title/" + id;
    }

    @GetMapping("/new-title/{id}")
    public String updateTitleId(@PathVariable String id, Model model) {
        Title title = titleService.findById(Long.valueOf(id));
        TitleCommand titleCommand = titleToTitleCommand.convert(title);
        getTitleParams(model, titleCommand);
        return TITLE_FORM;
    }

    @GetMapping("/new-title")
    public String newTitle(@RequestParam(name = "id", required = false) String authorId, Model model) {
        TitleCommand titleCommand = new TitleCommand();
        if (authorId != null) {
            titleCommand.setAuthors(new HashSet<>(Collections.singletonList(authorService.findById(Long.valueOf(authorId)))));
        }
//        getTitleParams(model, titleCommand);
        model.addAttribute(TITLE, titleCommand);
        model.addAttribute(EDITORS, editorService.findAll());
        model.addAttribute(ISBNS, isbnService.findAll());
        model.addAttribute(AUTHORS, authorService.findAll());
        model.addAttribute(TITLES, titleService.findAll());
        model.addAttribute(TAGS, tagService.findAll());
        model.addAttribute(SECTIONS, sectionService.findAll());
        return TITLE_FORM;
    }

    private void getTitleParams(Model model, TitleCommand titleCommand) {
        model.addAttribute(TITLE, titleCommand);
        model.addAttribute(EDITORS, editorService.findAll());
        model.addAttribute(ISBNS, isbnService.findAll());
        model.addAttribute(AUTHORS, authorService.findAll());
        model.addAttribute(TITLES, titleService.findAll());
        model.addAttribute(TAGS, tagService.findAll());
        model.addAttribute(SECTIONS, sectionService.findAll());
    }

    @GetMapping("/get-new-catNumber")
    public String newCatalogNumber(Model model) {
        model.addAttribute("catNumber", titleService.findFirstFreeCatalogNumber().toString());
        return "titles/cat-number";
    }


    @PostMapping("/add-title")
    public String addTitle(@ModelAttribute String title, Model model) {
        TitleCommand titleCommand = new TitleCommand();
        titleCommand.setName(title);
        model.addAttribute(TITLE, titleCommand);
        return TITLE_FORM;
    }

    @PostMapping("/add-new-title")
    public String saveOrUpdate(@ModelAttribute TitleCommand command) {
        TitleCommand savedCommand = titleService.saveTitleCommand(command);
        return "redirect:/titles/show/" + savedCommand.getId();
    }

    @GetMapping("/move-author")
    public String moveAuthor() {
        titleService.moveAuthors();
        return TITLES_INDEX;
    }

    @PostMapping("/search-by-string")
    public String searchTitleByString(@ModelAttribute SearchField searchField, Model model) {
        String searchText = searchField.getSearchText();
        List<Title> titleList = titleService.searchTitlesContainingString(searchText);
        titleList.sort(Comparator.comparing(Title::getName, String::compareToIgnoreCase));
        model.addAttribute(TITLES, titleList);
        Title title = new Title();
        title.setName(searchText);
        model.addAttribute(TITLE, title);
        return "titles/found";
    }
}