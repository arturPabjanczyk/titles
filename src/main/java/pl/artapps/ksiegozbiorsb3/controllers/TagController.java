package pl.artapps.ksiegozbiorsb3.controllers;

import org.springframework.stereotype.*;
import org.springframework.ui.*;
import org.springframework.web.bind.annotation.*;
import pl.artapps.ksiegozbiorsb3.commands.*;
import pl.artapps.ksiegozbiorsb3.model.*;
import pl.artapps.ksiegozbiorsb3.services.*;

import java.util.*;

/**
 * created by ap on 23/1/19
 */

@RequestMapping("/tags")
@Controller
public class TagController {

    private final TagService tagService;
    private final TitleService titleService;

    public TagController(TagService tagService, TitleService titleService) {
        this.tagService = tagService;
        this.titleService = titleService;
    }

    @GetMapping({"", "/", "index", "index.html"})
    public String getTags(Model model) {
        List<Tag> tagList = tagService.findAll();
        tagList.sort(Comparator.comparing(Tag::getName, String::compareToIgnoreCase));
        model.addAttribute("tags", tagList);
        model.addAttribute("searchField", new SearchField());
        return "tags/index";
    }

    @PostMapping("/search-by-string")
    public String searchSectionByString(@ModelAttribute SearchField searchField, Model model) {
        String searchText = searchField.getSearchText();
        List<Tag> tagList = tagService.searchSectionsContainingString(searchText);
        tagList.sort(Comparator.comparing(Tag::getName, String::compareToIgnoreCase));
        model.addAttribute("tags", tagList);
        Tag tag = new Tag();
        tag.setName(searchText);
        model.addAttribute("tag", tag);
        return "tags/found";
    }

    @PostMapping("/add-new-tag")
    public String saveOrUpdate(@ModelAttribute TagCommand command, Model model) {
        TagCommand savedCommand = tagService.saveTagCommand(command);
        return "redirect:/tags/show/" + savedCommand.getId();
    }


    @GetMapping("/show/{id}")
    public String showTagById(@PathVariable Long id, Model model) {
        Tag tag = tagService.findById(id);
        model.addAttribute("tag", tag);
        List<Title> titleList = titleService.findTitlesByTagId(id);
        titleList.sort(Comparator.comparing(Title::getName, String::compareToIgnoreCase));
        model.addAttribute("titles", titleList);
        return "tags/show";
    }
}
