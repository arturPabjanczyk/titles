package pl.artapps.ksiegozbiorsb3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.artapps.ksiegozbiorsb3.model.Format;

import java.util.Optional;

/**
 * created by ap on 22/1/19
 */

@Repository
public interface FormatRepository extends JpaRepository<Format, Long> {
    Optional<Format> findByname(String name);
}