package pl.artapps.ksiegozbiorsb3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.artapps.ksiegozbiorsb3.model.Author;
import pl.artapps.ksiegozbiorsb3.model.Tag;
import pl.artapps.ksiegozbiorsb3.model.Title;

import java.util.List;

/**
 * created by ap on 12/1/19
 */

@Repository
public interface TitleRepository extends JpaRepository<Title, Long> {

    List<Title> findByName(String name);

    List<Title> findBySectionId(Long id);

    List<Title> findByNameContains(String searchText);

    List<Title> findByTagsContaining(Tag tag);

    List<Title> findByEditorId(Long id);

    List<Title> findTitleByAuthorsContains(Author author);
}
