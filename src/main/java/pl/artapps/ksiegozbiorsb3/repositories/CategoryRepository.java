package pl.artapps.ksiegozbiorsb3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.artapps.ksiegozbiorsb3.model.Category;

import java.util.List;
import java.util.Optional;

/**
 * created by ap on 22/1/19
 */

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    Optional<Category> findByName(String name);
    List<Category> findByNameContains(String searchText);
}