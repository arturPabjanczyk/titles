package pl.artapps.ksiegozbiorsb3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.artapps.ksiegozbiorsb3.model.Isbn;

import java.util.Optional;

/**
 * created by ap on 23/1/19
 */

@Repository
public interface IsbnRepository extends JpaRepository<Isbn, Long> {
    Optional<Isbn> findByIsbnNumber(String isbnNumber);
}