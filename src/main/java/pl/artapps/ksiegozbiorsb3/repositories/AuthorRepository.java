package pl.artapps.ksiegozbiorsb3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.artapps.ksiegozbiorsb3.model.Author;

import java.util.List;
import java.util.Optional;

/**
 * created by ap on 12/1/19
 */

@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
    Optional<Author> findByLastName(String lastName);
    Optional<Author> findByFirstNameAndLastName(String firstName, String lastName);
    List<Author> findByFirstNameContains(String searchText);
    List<Author> findByLastNameContains(String searchText);
}