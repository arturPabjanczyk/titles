package pl.artapps.ksiegozbiorsb3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.artapps.ksiegozbiorsb3.model.Tag;

import java.util.List;
import java.util.Optional;

/**
 * created by ap on 23/1/19
 */

@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

    Optional<Tag> findByName(String name);

    List<Tag> findByNameContains(String searchText);
}