package pl.artapps.ksiegozbiorsb3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.artapps.ksiegozbiorsb3.model.Section;

import java.util.List;
import java.util.Optional;

/**
 * created by ap on 22/1/19
 */

@Repository
public interface SectionRepository extends JpaRepository<Section, Long> {

    Optional<Section> findByName(String name);

    List<Section> findByNameContains(String searchText);
}