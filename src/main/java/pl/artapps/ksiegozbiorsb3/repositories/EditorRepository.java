package pl.artapps.ksiegozbiorsb3.repositories;

import org.springframework.data.jpa.repository.*;
import pl.artapps.ksiegozbiorsb3.model.*;

import java.util.*;

/**
 * created by ap on 12/1/19
 */

//@Repository
public interface EditorRepository extends JpaRepository<Editor, Long> {
    Optional<Editor> findByName(String name);
}