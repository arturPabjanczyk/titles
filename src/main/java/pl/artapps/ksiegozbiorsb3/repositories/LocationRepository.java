package pl.artapps.ksiegozbiorsb3.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.artapps.ksiegozbiorsb3.model.Location;

import java.util.Optional;

/**
 * created by ap on 22/1/19
 */

@Repository
public interface LocationRepository extends JpaRepository<Location, Long> {
    Optional<Location> findByName(String name);
}