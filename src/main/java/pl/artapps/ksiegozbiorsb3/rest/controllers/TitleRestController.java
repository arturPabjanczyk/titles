package pl.artapps.ksiegozbiorsb3.rest.controllers;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.artapps.ksiegozbiorsb3.model.Author;
import pl.artapps.ksiegozbiorsb3.model.Title;
import pl.artapps.ksiegozbiorsb3.services.AuthorService;
import pl.artapps.ksiegozbiorsb3.services.TitleService;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * created by ap on 12/1/19
 */

@RequestMapping("/rest-titles")
@RestController
public class TitleRestController {

    private final TitleService titleService;
    private final AuthorService authorService;

    public TitleRestController(TitleService titleService, AuthorService authorService) {
        this.titleService = titleService;
        this.authorService = authorService;
    }

    @GetMapping(value = "/titles_json", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Title> getTitles() {
        List<Title> titleList = titleService.findAll();
        titleList.sort(Comparator.comparing(Title::getName, String::compareToIgnoreCase));
        return titleList;
    }

    @PostMapping(value = "/author_chunk/{authorNameChunk}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<Author, List<Title>> getTitleByAuthorName(@PathVariable String authorNameChunk) {
        List<Author> authors = authorService.findByNameChunk(authorNameChunk);
        Map<Author, List<Title>> authorArrayListMap = new HashMap<>();
        authors.forEach(author -> authorArrayListMap.put(author, titleService.findByAuthor(author)));
        return authorArrayListMap;
    }
}