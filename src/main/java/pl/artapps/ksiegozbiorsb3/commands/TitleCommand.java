package pl.artapps.ksiegozbiorsb3.commands;

import lombok.*;
import pl.artapps.ksiegozbiorsb3.model.*;

import java.util.*;

/**
 * created by ap on 25/1/19.
 */

@Setter
@Getter
@NoArgsConstructor
public class TitleCommand {
    private Byte[] image;
    private Editor editor;
    private int numberOfPages;
    private Isbn isbn;
    private Long catNumber;
    private Set<Author> authors = new HashSet<>();
    private Set<Format> formats;
    private Set<Location> locations;
    private Set<Tag> tags;
    private Set<Title> subtitles;
    private Set<Translator> translators = new HashSet<>();
    private Section section;
    private String id;
    private String link;
    private String notes;
    private String originalTitle;
    private String overtitle;
    private String ratingString;
    private String releaseYear;
    private String review;
    private String name;
    private String titleSecondPart;
    private String whenRead;
    private Rating rating;
}
