package pl.artapps.ksiegozbiorsb3.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * created by ap on 25/1/19.
 */

@Setter
@Getter
@NoArgsConstructor
public class TagCommand {
    private Long id;
    private String name;
}
