package pl.artapps.ksiegozbiorsb3.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.artapps.ksiegozbiorsb3.model.Title;

/**
 * created by ap on 25/1/19.
 */

@Setter
@Getter
@NoArgsConstructor
public class ReviewCommand {
    private Long id;
    private Title title;
    private String review;
}
