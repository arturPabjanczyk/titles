package pl.artapps.ksiegozbiorsb3.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * created by ap on 22/4/19.
 */

@Setter
@Getter
@NoArgsConstructor
public class SearchField {
    private String searchText;
}