package pl.artapps.ksiegozbiorsb3.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.artapps.ksiegozbiorsb3.model.Title;

import java.util.HashSet;
import java.util.Set;

/**
 * created by ap on 25/1/19.
 */

@Setter
@Getter
@NoArgsConstructor
public class IsbnCommand {
    private Long id;
    private String isbnNumber;
    private Set<Title> titles = new HashSet<>();
}
