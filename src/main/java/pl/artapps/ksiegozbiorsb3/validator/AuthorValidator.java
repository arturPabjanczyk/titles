package pl.artapps.ksiegozbiorsb3.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import pl.artapps.ksiegozbiorsb3.model.Author;

public class AuthorValidator implements Validator {

    public boolean supports(Class clazz) {
        return Author.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors e) {
        ValidationUtils.rejectIfEmpty(e, "name", "name.empty");
        Author a = (Author) obj;
        if (a.getTitles().isEmpty()) {
            e.rejectValue("titles", "no title");
        }
    }
}