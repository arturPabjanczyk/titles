package pl.artapps.ksiegozbiorsb3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ksiegozbiorsb3Application {

    public static void main(String[] args) {
        SpringApplication.run(Ksiegozbiorsb3Application.class, args);
    }

}

