package pl.artapps.ksiegozbiorsb3.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * created by ap on 11/1/19
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "titles")
public class Title extends NamedEntity {

    private Long catNumber;
    private boolean isShortStory;
    private String link;//
    private String originalTitle;//
    private int numberOfPages;//
    private String releaseYear;//
    private String titleSecondPart;//
    private String whenRead;//
    private String notes;

    @Lob
    private Byte[] image;

    @Lob
    private String review;

    @OneToMany
    @JoinColumn(name = "title_id")
    private Set<Quotation> quotations = new HashSet<>();

    @ManyToOne
    private Editor editor;

    @ManyToOne
    private Section section;

    @ManyToOne
    private Rating rating;

    @ManyToOne
    private Isbn isbn;

    @JsonBackReference
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "author_title",
            joinColumns = @JoinColumn(name = "title_id"),
            inverseJoinColumns = @JoinColumn(name = "author_id"))
    private Set<Author> authors = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "tag_title",
            joinColumns = @JoinColumn(name = "title_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id"))
    private Set<Tag> tags = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "translator_title",
            joinColumns = @JoinColumn(name = "title_id"),
            inverseJoinColumns = @JoinColumn(name = "translator_id"))
    private Set<Translator> translators = new HashSet<>();

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "category_title",
            joinColumns = @JoinColumn(name = "title_id"),
            inverseJoinColumns = @JoinColumn(name = "category_id"))
    private Set<Category> categories;

//    @OneToMany
//    @JoinTable(name = "title_title",
//            joinColumns = @JoinColumn(name = "title_id"),
//            inverseJoinColumns = @JoinColumn(name = "subtitle_id"))
//    private Set<Title> subtitles;

//    @ManyToOne
//    @JoinTable(name = "title_title",
//            joinColumns = @JoinColumn(name = "subtitle_id"),
//            inverseJoinColumns = @JoinColumn(name = "title_id"))
//    private Title overtitle;

    @ManyToMany
    @JoinTable(name = "format_title",
            joinColumns = @JoinColumn(name = "title_id"),
            inverseJoinColumns = @JoinColumn(name = "format_id"))
    private Set<Format> formats;

    @ManyToMany
    @JoinTable(name = "location_title",
            joinColumns = @JoinColumn(name = "title_id"),
            inverseJoinColumns = @JoinColumn(name = "location_id"))
    private Set<Location> locations;

}
