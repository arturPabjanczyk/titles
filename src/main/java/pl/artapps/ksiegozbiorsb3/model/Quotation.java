package pl.artapps.ksiegozbiorsb3.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * created by ap on 21/1/19
 */

@Setter
@Getter
@Entity
@Table(name = "quotations")
public class Quotation extends NamedEntity {

    @ManyToOne
    private Title title;
}
