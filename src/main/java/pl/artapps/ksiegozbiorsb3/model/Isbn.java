package pl.artapps.ksiegozbiorsb3.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * created by ap on 23/1/19
 */

@Getter
@Setter
@Entity
@Table(name = "Isbns")
public class Isbn extends BaseEntity {

    private String isbnNumber;

    @JsonBackReference
    @OneToMany
    @JoinColumn(name = "isbn_id")
    private Set<Title> titles = new HashSet<>();

    @Override
    public String toString() {
        return isbnNumber;
    }
}
