package pl.artapps.ksiegozbiorsb3.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * created by ap on 19/4/19
 */

@Setter
@Getter
@Entity
@Table(name = "ratings")
public class Rating extends NamedEntity {
    @JsonBackReference
    @OneToMany
    @JoinColumn(name = "rating_id")
    private Set<Title> titles = new HashSet<>();
}
