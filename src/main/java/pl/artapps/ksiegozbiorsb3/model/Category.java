package pl.artapps.ksiegozbiorsb3.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * created by ap on 21/1/19
 */

@Getter
@Setter
@Entity
@Table(name = "categories")
public class Category extends NamedEntity {

    @JsonBackReference
    @ManyToMany(mappedBy = "categories")
    private Set<Title> titles = new HashSet<>();
}
