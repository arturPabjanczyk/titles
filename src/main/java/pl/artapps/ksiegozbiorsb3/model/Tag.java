package pl.artapps.ksiegozbiorsb3.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * created by ap on 23/1/19
 */

@Getter
@Setter
@Entity
@Table(name = "tags")
public class Tag extends NamedEntity {

    @JsonBackReference
    @ManyToMany(mappedBy = "tags")
    private Set<Title> titles = new HashSet<>();
}