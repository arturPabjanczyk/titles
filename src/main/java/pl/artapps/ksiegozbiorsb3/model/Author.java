package pl.artapps.ksiegozbiorsb3.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * created by ap on 11/1/19
 */

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "authors")
public class Author extends PersonEntity {

    @JsonBackReference
    @ManyToMany(mappedBy = "authors")
    private Set<Title> titles = new HashSet<>();
}