package pl.artapps.ksiegozbiorsb3.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * created by ap on 11/1/19
 */

@Getter
@Setter
@Entity
@Table(name = "editors")
public class Editor extends NamedEntity {

    private String city;

    @JsonBackReference
    @OneToMany
    @JoinColumn(name = "editor_id")
    private Set<Title> titles = new HashSet<>();
}
