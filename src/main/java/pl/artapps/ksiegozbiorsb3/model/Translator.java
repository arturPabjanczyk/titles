package pl.artapps.ksiegozbiorsb3.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * created by ap on 11/1/19
 */

@Entity
@Table(name = "translators")
public class Translator extends PersonEntity {

    @JsonBackReference
    @ManyToMany(mappedBy = "translators")
    private Set<Title> titles = new HashSet<>();
}