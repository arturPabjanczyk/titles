package pl.artapps.ksiegozbiorsb3.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * created by ap on 21/1/19
 */

@Getter
@Setter
@Entity
@Table(name = "sections")
public class Section extends NamedEntity {

    @JsonBackReference
    @OneToMany(mappedBy = "section")
    private Set<Title> titles = new HashSet<>();
}